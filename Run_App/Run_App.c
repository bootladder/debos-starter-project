#include "Run_App.h"
#include "AppSystemCheckpoint.h"

#define CHECKPOINT_FLAG_LED 0x10
#define CHECKPOINT_FLAG_PRINTMSG 0x8
#define CHECKPOINT_FLAG_PRINTADDR 0x2
void Init_App(void)
{
    AppSystemWriteCheckpointBuffer((uint8_t*)"Init_App()\n", 12);
    AppSystemCheckpoint(0x30303A33, 0x2|0x8);
}

void Run_App(void)
{
    return;
}
